package lab02

object Recursion {

  def fibonacci(n:Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fibonacci(n-1) + fibonacci(n-2)
  }
}
