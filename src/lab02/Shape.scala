package lab02

trait Shape
object Shape{

  case class Rectangle(base: Double, height: Double) extends Shape
  case class Circle(radius: Double) extends Shape
  case class Square(base: Double) extends Shape

  def perimeter(s: Shape): Double = s match {
    case Rectangle(b, h) => (b+h)*2
    case Circle(r) => 2*r*math.Pi
    case Square(p) => p*4
  }

  def area(s: Shape): Double = s match {
    case Rectangle(b, h) => b*h
    case Circle(r) => r*r*math.Pi
    case Square(b) => b*b
  }
}
