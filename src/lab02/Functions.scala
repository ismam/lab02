package lab02

object Functions {

  /* Part 2a - 3a */
  val parity: Int => String = {
    case n if(n%2==0) => "even"
    case _ => "odd"
  }

  /* Part 2a - 3b */
  val empty : String => Boolean = _ == ""
  val neg:(String => Boolean) => (String => Boolean) =
    f => (s => s.nonEmpty)
  val notEmpty: String => Boolean = neg(empty);

  /* Part 2b - 4 */
  val valCurrying: Int=>Int=>Int => Boolean = { x=>y=>z => x<=y && y <=z }
  val valNoCurrying: (Int, Int, Int) => Boolean = { (x,y,z) => x<=y && y <=z }
  def defCurrying(x: Int)(y: Int)(z: Int): Boolean = { x<=y && y <=z }
  def defNoCurrying(x:Int,y:Int,z:Int) : Boolean = { x<=y && y <=z }

  /* Part 2b - 5 */
  def compose(f: Int => Int, g: Int => Int) : Int => Int = { i => f(g(i))}
  val exp : Int => Int = f => f*f
  val composedExp : Int => Int = compose(exp,exp)


}
