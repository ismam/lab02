package lab02Test

import lab02.Functions
import lab02.Functions._
import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertNotEquals, assertTrue}
import org.junit.jupiter.api.Test

class FunctionTest {

  @Test def testParity(){
    assertEquals("even", parity(4))
    assertEquals("odd",parity(11))
    assertNotEquals("even", parity(33))
  }

  @Test def testEmpty(): Unit ={
    assertTrue(Functions.empty(""))
    assertFalse(Functions.empty("a"))
  }

  @Test def testNeg(): Unit ={
    assertTrue(notEmpty("foo"))
    assertFalse(notEmpty(""))
  }

  @Test def testCurrying(): Unit ={
    assertTrue(valCurrying(12)(20)(54))
    assertTrue(valNoCurrying(6,50,73))
    assertTrue(defCurrying(1)(2)(3))
    assertTrue(defNoCurrying(9,17,23))
  }

  @Test def testComposition(): Unit ={
    assertEquals(composedExp(3), 81)
    assertEquals(composedExp(2), 16)
  }

  
}
