package lab02Test

import lab02.Recursion.fibonacci
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RecursionTest {

  @Test
  def fibonacciTest(): Unit ={
    assertEquals(fibonacci(10),55)
    assertEquals(fibonacci(1),1)
    assertEquals(fibonacci(0),0)
  }
}
