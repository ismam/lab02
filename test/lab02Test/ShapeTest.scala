package lab02Test

import lab02.Shape.{Circle, Rectangle, Square}
import lab02.Shape
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ShapeTest {

  @Test
  def rectangleTest(): Unit ={
    assertEquals(Shape.perimeter(Rectangle(5,10)),30)
    assertEquals(Shape.area(Rectangle(5,10)),50)
  }

  @Test
  def circleTest(): Unit ={
    assertEquals(Shape.perimeter(Circle(5)),2*5*math.Pi)
    assertEquals(Shape.area(Circle(5)),5*5*math.Pi)
  }

  @Test
  def squareTest(): Unit ={
    assertEquals(Shape.perimeter(Square(5)),20)
    assertEquals(Shape.area(Square(5)),25)
  }
}